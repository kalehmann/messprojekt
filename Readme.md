# Messprojekt

Das Messprojekt dient zum Messen und Aufzeichnen von Spannungen mit einem
Raspberry Pi.

In diesem Repository befindet sich auch die [Dokumentation des Projektes](/Dokumentation/Dokumentation.md).

Außerdem habe ich auf [meinem Blog](https://blog.kalehmann.de/) beschrieben
wie man mit dem Messprojekt [die Temperatur an einem Ursamar RK44 Temperaturregler ausliest](https://blog.kalehmann.de/blog/2018/07/30/automatische-temperaturerfassung-mit-ursamar-rk44.html)

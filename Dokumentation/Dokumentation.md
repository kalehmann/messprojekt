# Messprojekt

## Inhalt
1. Einleitung
2. Einrichtung des Raspberry Pis
3. Installation des Messprojektes
4. Verbinden der Hardware
5. Konfiguration des Messprojektes
6. Verwendung des Messprojektes

## 1. Einleitung

Diese Projekt ermöglicht es mittels eines Analog-Digital-Wandlers
Spannungswerte an einem Raspberry Pi aufzunehmen und in Excel-Tabellen
zu speichern.

Zusätzlich gibt es ein Display und 6 Hardwaretasten um das Programm zu
steuern.

## 2. Einrichtung des Raspberry Pis

### 2.1 Installation des Betriebssystems
Es wird die minimale Variante von Raspbian empfohlen. Diese kann auf der
Website der Raspberry Pi Foundation heruntergeladen werden.

Das heruntergeladene Image kann dann mit folgendem Befehl auf die SD-Karte
geschrieben werden:  
`dd if=raspbian.img of=/dev/mmcblk0 bs=1M`  
Nun müssen noch ein paar Dateien modifiziert werden.

#### Auf der **boot** Partition
Hier wird die Datei **ssh** angelegt. Damit wird der SSH-Server automatisch
gestartet.
Anschließend fügt man in der Datei **config.txt** folgendes ein:
```
# I2C aktivieren
dtparam=i2c1=on
dtparam=i2c_arm=on
```
#### Auf der **rootfs** Partition
In der Datei **/etc/modules** fügt man folgendes ein:
```
i2c-bcm2705
i2c-dev
```
Zusätzlich weist man dem Raspberry Pi für die Ethernetschnittstelle eine
statische IP-Adresse zu. Dazu fügt man in der Datei **/etc/dhcpcd.conf**
folgendes ein:
```
interface eth0
static ip_address=192.168.1.100/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1
```

### 2.2. Installation der benötigten auf dem Raspberry Pi
Nachdem man sich mit dem Raspberry Pi verbunden hat, führt man dort
folgende Befehle aus:
```
sudo su
apt update
apt install -y i2c-tools python3-rpi.gpio python3-pip python3-pil python3-smbus git
git clone https://github.com/adafruit/Adafruit_Python_SSD1306.git
cd Adafruit_Python_SSD1306
python3 setup.py install
cd ..
rm -rf Adafruit_Python_SSD1306
git clone https://github.com/adafruit/Adafruit_Python_ADS1x15.git
cd Adafruit_Python_ADS1x15
python3 setup.py install
cd ..
rm -rf Adafruit_Python_ADS1x15
```

## 3. Installation des Messprojektes
### 3.1 Die Dateien an die richtige Stelle kopieren
Zuerst erstellt man das Verzeichnis **/home/pi/usb1** und fŭgt in
der Datei **/etc/fstab** folgendes ein:
```
/dev/sda1  /home/pi/usb1  vfat  defaults,umask=01,uid=pi,gid=pi  0  0
```
Jetzt kopiert man das Messprojekt auf einen Fat-formatierten USB Stick in
einen Ordner mit dem Namen **messprojekt**.  
Diesen USB Stick steckt man nun an den Raspberry Pi an und führt dort
`mount -a` aus.

### 3.2 Den Autostart und das Logging des Messprojektes einrichten
Dazu kopiert man zuerst die Datei **messprojekt.service** aus dem
**Projekt** Ordner nach **/etc/systemd/system**.
Danach führt man `systemctl enable messprojekt.service` aus.
Jetzt kopiert man noch die Datei **99-messprojekt.conf** aus dem
**Projekt** Ordner nach **/etc/rsyslog.d**.  
Nach einem Neustart sollte das Messprojekt auch schon laufen.

## 4. Verbinden der Hardware
- Zuerst stellt man sicher, dass die Stromquelle noch ausgeschalten ist.
- Dann verbindet man das Kabel mit der Stromquelle und dem Messgerät.
- Jetzt überprüft man, ob das schwarz markierte Ende des Kabels wirklich an die Masse angeschlossen ist.
- Nun können Raspberry Pi und Stromquelle gestartet werden.

## 5. Konfiguration des Messprojektes
Zur Konfiguration muss die Datei **Vorlage/Vorlage.xlxs** bearbeitet
werden.
In ihr befindet sich die Tabelle **Einstellungen**, in welcher der
Messabstand in Sekunden eingestellt werden kann.

## 6. Verwendung des Messprojektes
### 6.1. Navigation in der Benutzeroberfläche
Mit den Tasten 4  (unten links) und 5 (unten rechts) kann zwischen den
einzelnen Seiten der Benutzeroberfläche gewechselt werden.

Auf manchen Seiten - zum Beispiel der Seite für den Messabstand oder der
Seite zum Herunterfahren und Neustarten - kann man mit den Tasten 2 (oben
mitte) und 4 (unten mitte) zusätzlich navigieren.

Weiterhin lassen sich Optionen auf manchen Seiten mit der Taste 1 (oben
links) bestätigen. Somit lässt sich zum Beispiel eine Messung starten.
![Die Nutzerschnittstelle](Nutzerschnittstelle.svg)

### 6.2. Messungen aufnehmen
Auf der Messseite sieht man die Id der aktuellen Messung und die Optionen
eine Messung zu starten oder zu beenden. Beim Beenden einer Messung werden
die erhobenen Daten im Ordner **Messreihen** in einer Excel Datei mit der
Id im Namen gespeichert.

### 6.3. Auswerten der Messreihen
Die Daten werden in der Excel-Datei in einer Tabelle mit dem Namen
**Rohdaten** abgespeichert.

Diese Tabelle hat 3 Spalten. In der ersten Spalte steht die Zeit seit
beginn der Messung in Sekunden, in der zweiten Spalte die Rohdaten vom
ersten Eingang und in der dritten Spalte die Rohdaten vom zweiten
Eingang.

Wenn man die Daten auswerten möchte, benötigt man zuerst eine Formel um
die Rohdaten in Sinnvolle Werte (Spannung, Temperatur ...) umzuwandeln.
Dazu empfiehlt es sich, die Rohwerte auf der Statusseite mit den
Ist-Werten für Spannung, Temperatur ... abzugleichen und so eine Formel
zu erstellen.

Dann erstellt man eine neue Tabelle, in welcher man auf die Rohdaten
verweist und diese mit der gefundenen Formel umwandelt.

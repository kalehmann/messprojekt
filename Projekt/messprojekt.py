#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

import RPi.GPIO as gpio
import time
import os
from subprocess import Popen
import inspect
import sys
import Adafruit_SSD1306
import Adafruit_ADS1x15
import openpyxl

from PIL import ImageFont, ImageDraw, Image

PROJECT_DIR = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe()))) + "/.."
TEMPLATE_PATH = PROJECT_DIR + "/Vorlage/Vorlage.xlsx"
DATA_DIR = PROJECT_DIR + "/Messreihen"

class Event(object):
    """Ein Event im Messprojekt. Dient hauptsächlich zur Kommunikation der
    einzelnen Module untereinander.

    Args:
        _type (str): Gibt den primären Typen des Ereignis an. Die im Programm
                     gängigen Typen sind die statischen Attribute dieser Klasse
                     mit dem Präfix TYPE_
        payload: Der dem Ereignis beigefügte Anhang
        subtype (str): Der optionale Subtyp des Ereignis. Die im Programm
                       gängigen Subtypen sind die statischen Attribute dieser
                       Klasse mit dem Präfix SUBTYPE_
    """
    TYPE_USERINPUT = "USER_INPUT"
    TYPE_MEASUREMENT = "MEASUREMENT"
    TYPE_STATUS = "STATUS"
    TYPE_CONFIG_CHANGED = "CONFIG_CHANGED"

    SUBTYPE_ID = "ID"
    SUBTYPE_CHANNEL_1 = "CHANNEL_1"
    SUBTYPE_CHANNEL_2 = "CHANNEL_2"
    SUBTYPE_START = "START"
    SUBTYPE_STOP = "STOP"
    SUBTYPE_DELAY = "DELAY"

    SUBTYPE_KEYDOWN = "KEYDOWN"
    SUBTYPE_KEYPRESSED = "KEYPRESSED"
    SUBTYPE_KEYUP = "KEYUP"

    def __init__(self, _type, payload, subtype=None):
        self.type = _type
        self.subtype = subtype
        self.payload = payload

    def __str__(self):
        """Wandelt das Ereignis in eine Zeichenkette mit Typ, Subtyp und
        Anhang um.
        """
        if self.subtype:
            return "%s %s:%s" % (self.type, self.subtype, self.payload)
        return "%s: %s" % (self.type, self.payload)

    def __repr__(self):
        """Gibt eine menschenlesbare Repräsentation des Ereignis zurück.
        Siehe __str__
        """
        return self.__str__()

class Page(object):
    """Basisklasse für eine Seite im Displaymodul des Messprojektes"""
    def __init__(self):
        self.size = 128, 64
        self.image = Image.new('1', self.size)
        self.draw = ImageDraw.Draw(self.image)
        self.font = ImageFont.load_default()

    def activate(self):
        """Diese Methode wird aufgerufen, wenn der Nutzer die Seite aktiviert,
        d. h. auf der Seite den Button 1 betätigt.

        Returns:
            list: Eine Liste mit Ereignissen, die dadurch ausgelösst werden.
        """
        return []

    def back(self):
        """Diese Methode wird aufgerufen, wenn der Nutzer auf der Seite den
        Button 3 betätigt. Dieser Button ist mit der zurück Funktion assoziiert.

        Returns:
            list: Eine Liste mit Ereignissen, die dadurch ausgelösst werden.
        """
        return []

    def up(self):
        """Diese Methode wird aufgerufen, wenn der Nutzer auf der Seite den
        Button 3 betätigt. Dieser Button ist mit einer Navigation nach oben
        assoziiert.

        Returns:
            list: Eine Liste mit Ereignissen, die dadurch ausgelösst werden.
        """
        return []

    def down(self):
        """Diese Methode wird aufgerufen, wenn der Nutzer auf der Seite den
        Button 3 betätigt. Dieser Button ist mit einer Navigation nach unten
        assoziiert.

        Returns:
            list: Eine Liste mit Ereignissen, die dadurch ausgelösst werden.
        """
        return []

    def clear(self):
        """Diese Methode setzt den Inhalt der Seite zurück."""
        self.draw.rectangle((0,0, *self.size), fill=0)

    def write(self, position, text):
        """Diese Methode schreibt Text mit weißer Schrift auf die Seite.

        Args:
            position (tuple): Die Position, an welcher der Text anfangen soll
            text (str): Der Text, welcher auf die Seite geschrieben werden soll.
        """
        self.draw.text(position, text, font=self.font, fill=255)

    def update(self, events):
        """Diese Methode gibt der Seite die Möglichkeit, Ereignisse zu
        verarbeiten.

        Args:
            events (list): Eine Liste mit Ereignissen.

        Notes:
            In der Implementation dieser Methode soll kein Rendering
            stattfinden. Dafür existiert die Methode render.

            Diese Methode wird auch für inaktive Seiten aufgerufen.
        """
        pass

    def render(self):
        """Diese Methode wird zum Rendering der Seite genutzt. Sie wird nur
        aufgerufen, wenn die Seite gerade aktiv ist.
        """
        pass

class MessProjekt(object):
    """Die Hauptklasse des Messprojektes. Sie verwaltet die verschiedenen Module
    des Projektes.
    """
    def __init__(self):
        self.modules = []

    def setup(self):
        """Initialisiert alle Module des Projektes"""
        for module in self.modules:
            module.setup()

    def update(self):
        """Aktualisiert alle Module und lässt die Module die dabei generierten
        Ereignisse verarbeiten"""
        events = []
        for module in self.modules:
            events += module.update()
        print(events)
        for module in self.modules:
            module.handle(events)

    def cleanup(self):
        """Lässt alle Module aufräumen."""
        print ("Cleaning up")
        for module in self.modules:
            module.cleanup()

    def run(self):
        """Diese Methode startet das Messprojekt"""
        self.setup()
        try:
            while True:
                self.update()
                time.sleep(.03)
        finally:
            self.cleanup()

class Module(object):
    """Basisklasse für ein Modul des Messprojektes"""
    def setup(self):
        """Diese Methode kann genutzt werden, um das Modul zu initialisieren"""
        pass

    def update(self):
        """in der Implemtation dieser Methode können Messungen getätigt und die
        daraus resultierenden Ereignisse zurückgegeben werden.

        Returns:
            list: Eine Liste mit eventuell entstandenen Ereignissen.
        """
        return []

    def handle(self, events):
        """Diese Methode bietet die Möglichkeit auf die Ereignisse anderer
        Module zu reagieren.

        Args:
            events (list): Eine Liste mit allen bei der Aktualisierung der
                           Module generierten Ereignissen.
        """
        pass

    def cleanup(self):
        """Diese Methode kann zum Aufräumen genutzt werden. Zum Beispiel um
        eventuell initialisierte Messhardware wieder frei zu geben.
        """
        pass

class GpioModule(Module):
    """Dieses Modul wird genutzt um Nutzereingaben mittels der 6 Hardware-
    Buttons zu verarbeiten.

    Dabei werden Ereignisse vom "TYPE_USERINPUT" generiert.
    """
    BUTTON_PINS = {
        4  : "BTN_1",
        15 : "BTN_2",
        18 : "BTN_3",
        14 : "BTN_4",
        17 : "BTN_5",
        27 : "BTN_6"
    }

    def __init__(self):
        self.state = {
           "BTN_1" : 0,
           "BTN_2" : 0,
           "BTN_3" : 0,
           "BTN_4" : 0,
           "BTN_5" : 0,
           "BTN_6" : 0
        }

    def setup(self):
        gpio.setmode(gpio.BCM)
        for pin in self.BUTTON_PINS.keys():
            gpio.setup(pin, gpio.IN, pull_up_down=gpio.PUD_UP)

    def update(self):
        res = []
        for pin in self.BUTTON_PINS.keys():
            btn = self.BUTTON_PINS[pin]
            if gpio.input(pin):
                if self.state[btn]:
                    res.append(Event(Event.TYPE_USERINPUT, btn, Event.SUBTYPE_KEYUP))
                    self.state[btn] = 0
            else:
                if self.state[btn]:
                    res.append(Event(Event.TYPE_USERINPUT, btn, Event.SUBTYPE_KEYPRESSED))
                else:
                    res.append(Event(Event.TYPE_USERINPUT, btn, Event.SUBTYPE_KEYDOWN))
                    self.state[btn] = 1
        return res

    def cleanup(self):
        gpio.cleanup()

class MessModule(Module):
    """Diese Modul wird genutzt um Spannungen an zwei Kanälen eines Analog-
    Digital-Wandlers zu messen.

    Dabei werden Events vom "TYPE_MEASUREMENT" generiert, welche die Rohdaten
    des ADCs (ads1115) beinhalten und Events vom "TYPE_STATUS", welche die
    Daten in einer Menschenlessbaren Form beinhalten.
    """
    def __init__(self):
        self.adc = None

    def setup(self):
        self.adc = Adafruit_ADS1x15.ADS1115()

    def getSpannung(self, wert):
        return wert * 1.0 / 2 ** 15 * 4 * 4

    def update(self):
        w0 = self.adc.read_adc(0, gain=1)
        w1 = self.adc.read_adc(1, gain=1)
        return [
            Event(Event.TYPE_MEASUREMENT, w0,
                  subtype=Event.SUBTYPE_CHANNEL_1),
            Event(Event.TYPE_MEASUREMENT, w1,
                  subtype=Event.SUBTYPE_CHANNEL_2),
            Event(Event.TYPE_STATUS, "Wert0: %d" % w0),
            Event(Event.TYPE_STATUS, "Wert1: %d" % w1),
            Event(Event.TYPE_STATUS, "V0: %02.2f V" % self.getSpannung(w0)),
            Event(Event.TYPE_STATUS, "V1: %02.2f V" % self.getSpannung(w1))
        ]

class DokumentationsModule(Module):
    """Dieses Modul dient zum Persistieren der erhobenen Messdaten. Dabei werden
    die aktuellen Messwerte in einem Konfigurierbaren Zeitabstand in eine Excel-
    Tabelle geschrieben.

    Die Steuerung des Dokumentationsmodules erfolgt dabei über Ereignisse.
    Ereignisse vom "TYPE_MEASUREMENT" mit dem "SUBTYPE_START" und dem
    "SUBTYPE_STOP" starten und stoppen die Dokumentation.
    Die einzelnen Messreihen werden in fortlaufend nummerierten Exceltabellen
    gespeichert.
    Wenn dabei die Nummer der nächsten Messreihe feststeht, wird ein Ereignis
    vom "TYPE_MEASUREMENT", dem "SUBTYPE_ID" und der Nummer als Anhang
    generiert.
    Der Abstand zwischen den einzelnen Messungen wird aus der Vorlagetabelle
    geladen. Er kann im laufenden Betrieb aktualisiert werden. Dies wird dem
    Messmodul über Ereignisse vom "TYPE_CONFIG_CHANGED", dem "SUBTYPE_DELAY" und
    der neuen Verzögerung in Sekunden im Anhang mitgeteilt.
    """
    def setup(self):
        try:
            self.workbook = openpyxl.load_workbook(TEMPLATE_PATH)
        except FileNotFoundError:
            raise Exception("Fehler, konnte die Datei %s nicht laden" %
                            TEMPLATE_PATH)
        self.worksheet = self.workbook["Rohdaten"]
        self.ws_settings = self.workbook["Einstellungen"]
        self.delay = self.ws_settings["B1"].value
        self.offset = 2
        self.count = 0
        self.last_measurement = 0
        self.measure = False
        self.measure_id = None

    def get_measure_id(self):
        _id = 0
        while True:
            if not os.path.isfile(self.get_out_path(_id)):
                return _id
            _id += 1

    def get_out_path(self, _id):
        return DATA_DIR + "/%04d.xlsx" % _id

    def save_workbook(self):
        self.workbook.save(filename = self.get_out_path(self.measure_id))
        self.measure_id = None

    def update(self):
        if self.measure_id == None:
            print("Generating id")
            self.measure_id = self.get_measure_id()
            return [Event(Event.TYPE_MEASUREMENT, self.measure_id,
                          subtype=Event.SUBTYPE_ID)]
        return []

    def handle(self, events):
        for event in events:
            if (event.type == Event.TYPE_MEASUREMENT and
                    event.subtype == Event.SUBTYPE_START):
                self.measure = True
            elif (event.type == Event.TYPE_MEASUREMENT and
                    event.subtype == Event.SUBTYPE_STOP):
                if self.measure:
                    self.save_workbook()
                self.measure = False
            elif (event.type == Event.TYPE_CONFIG_CHANGED and
                    event.subtype == Event.SUBTYPE_DELAY):
                self.delay = event.payload

        if not self.measure:
            return
        if time.time() - self.last_measurement < self.delay:
            return
        events = [ event for event in events
                     if event.type == Event.TYPE_MEASUREMENT]
        self.last_measurement = time.time()
        time_cell = self.worksheet["A%d" % (self.offset + self.count)]
        c1_cell = self.worksheet["B%d" % (self.offset + self.count)]
        c2_cell = self.worksheet["C%d" % (self.offset + self.count)]
        time_cell.value = self.count * self.delay
        for event in events:
            if event.subtype == Event.SUBTYPE_CHANNEL_1:
                c1_cell.value = event.payload
            elif event.subtype == Event.SUBTYPE_CHANNEL_2:
                c2_cell.value = event.payload
        self.count += 1

    def cleanup(self):
        if self.measure:
            self.save_workbook()

class EventPage(Page):
    """Eine Seite fÜr das Displaymodul, welche für Debuggingzwecke alle
    generierten Ereignisse ausgibt.
    """
    def update(self, events):
        self.events = events

    def render(self):
        self.clear()
        self.write((5,10), "Events:")
        y = 20
        for event in self.events:
            self.write((10, y), str(event))
            y += 10

class ShutdownPage(Page):
    """Diese Seite bietet dem Nutzer die Möglichkeit das Gerät neuzustarten oder
    auszuschalten.
    """
    def __init__(self):
        Page.__init__(self)
        self.selected = 0
        self.item_count = 1

    def up(self):
        self.selected = 0 if self.selected == 1 else 1
        return []

    def down(self):
        self.selected = 0 if self.selected == 1 else 1
        return []


    def activate(self):
        if self.selected == 0:
            Popen(["shutdown", "now", "-h"])
        elif self.selected == 1:
            Popen(["shutdown", "now", "-r"])
        sys.exit()

    def render(self):
        self.clear()
        self.write((5, 10 + self.selected * 10) , "*")
        self.write((10,10), "Herunterfahren")
        self.write((10,20), "Neustarten")

class StatusPage(Page):
    """Diese Seite gibt dem Nutzer die Anhänge von Ereignissen vom "TYPE_STATUS"
    aus. Somit können die Module dem Nutzer Daten ausgeben.
    """
    def update(self, events):
         is_status = lambda e: e.type == Event.TYPE_STATUS
         self.status_events = [e for e in events if is_status(e)]

    def render(self):
        self.clear()
        self.write((5,10), "Status")
        y = 20
        for event in self.status_events:
            self.write((10, y), event.payload)
            y += 10

class DelayPage(Page):
    def __init__(self):
        Page.__init__(self)
        try:
            self.workbook = openpyxl.load_workbook(TEMPLATE_PATH)
        except FileNotFoundError:
            raise Exception("Fehler, konnte die Datei %s nicht laden" %
                            TEMPLATE_PATH)
        self.ws_settings = self.workbook["Einstellungen"]
        self.delay = self.ws_settings["B1"].value

    def up(self):
        if self.delay >= 300:
            return []
        self.delay += 1
        self.ws_settings["B1"].value = self.delay
        self.workbook.save(filename = TEMPLATE_PATH)
        return [
            Event(Event.TYPE_CONFIG_CHANGED, self.delay,
                  subtype=Event.SUBTYPE_DELAY)
        ]

    def down(self):
        if self.delay <= 1:
            return []
        self.delay -= 1
        self.ws_settings["B1"].value = self.delay
        self.workbook.save(filename = TEMPLATE_PATH)
        return [
            Event(Event.TYPE_CONFIG_CHANGED, self.delay,
                  subtype=Event.SUBTYPE_DELAY)
        ]

    def render(self):
        self.clear()
        self.write((5, 10), "Zeit zwischen")
        self.write((5, 20), "Messungen:")
        self.write((15, 35), ("%d" % self.delay).rjust(3) +  " Sekunden")

class MeasurePage(Page):
    """Diese Seite lässt den Nutzer Messungen starten und stoppen. Außerdem gibt
    sie die Nummer der nächsten Messung aus.
    """
    def __init__(self):
        Page.__init__(self)
        self.measure = False
        self.measure_id = None

    def update(self, events):
        for event in events:
            if (event.type == Event.TYPE_MEASUREMENT and
                    event.subtype == Event.SUBTYPE_ID):
                self.measure_id = event.payload

    def render(self):
        self.clear()
        if self.measure_id != None:
            self.write((5, 10), "Id: %04d" % self.measure_id)
        else:
            self.write((5, 10), "Id: None")
        if self.measure:
            self.write((5, 25), "Messung beenden")
        else:
            self.write((5, 25), "Messung starten")


    def activate(self):
        self.measure = not self.measure
        if self.measure:
            return [Event(Event.TYPE_MEASUREMENT, '',
                          subtype=Event.SUBTYPE_START)]
        else:
            return [Event(Event.TYPE_MEASUREMENT, '',
                          subtype=Event.SUBTYPE_STOP)]

class DisplayModule(Module):
    """Dieses Modul zeigt dem Nutzer verschiedene Seiten an, mit denen er
    interagieren kann. Dabei können die Seiten auch eigene Events generieren um
    mit anderen Modulen zu kommunizieren.
    """
    def __init__(self):
        self.display = None
        self.pages = []
        self.current_page = 0
        self.page_events = []

    def setup(self):
        self.display = Adafruit_SSD1306.SSD1306_128_64(24)
        self.display.begin()
        self.display.clear()
        self.display.display()

    def update(self):
        events = self.page_events
        self.page_events = []
        return events

    def handle_userinput(self, events):
        for event in events:
            if (event.type == event.TYPE_USERINPUT and
                event.subtype == event.SUBTYPE_KEYDOWN and
                self.pages):
                if event.payload == 'BTN_1':
                    self.page_events += self.pages[self.current_page].activate()
                elif event.payload == 'BTN_2':
                    self.page_events += self.pages[self.current_page].up()
                elif event.payload == 'BTN_3':
                    self.page_events += self.pages[self.current_page].back()
                elif event.payload == 'BTN_4':
                    if self.current_page == 0:
                        self.current_page = len(self.pages) - 1
                    else:
                        self.current_page -= 1
                elif event.payload == 'BTN_5':
                    self.page_events += self.pages[self.current_page].down()
                elif event.payload == 'BTN_6':
                    if self.current_page == len(self.pages) - 1:
                        self.current_page = 0
                    else:
                        self.current_page += 1

    def handle(self, events):
        self.handle_userinput(events)
        if not self.pages:
            return
        for p in self.pages:
            p.update(events)
        page = self.pages[self.current_page]
        page.render()
        self.display.image(page.image)
        self.display.display()

    def cleanup(self):
        self.display.clear()
        self.display.display()

if __name__ == "__main__":
    p = MessProjekt()

    print("Starte das Messprojekt ...")

    p.modules.append(GpioModule())
    display_module = DisplayModule()
    #display_module.pages.append(EventPage())
    display_module.pages.append(StatusPage())
    display_module.pages.append(MeasurePage())
    display_module.pages.append(DelayPage())
    display_module.pages.append(ShutdownPage())
    p.modules.append(display_module)
    p.modules.append(MessModule())
    p.modules.append(DokumentationsModule())

    p.run()
